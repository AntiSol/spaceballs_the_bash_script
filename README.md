SPACEBALLS: The Bash Script
===========================

Outputs the script of the greatest movie ever made.

Ideal for use with xscreensaver's "starwars" hack.

Note that Spaceballs is trademark/copyright/etc somebody who isn't me. I don't *think* this infringes anything.

see ./spaceballs.sh --help for options and modes of operation, of which there are many.
